#include <stdio.h>

#include "pico/stdlib.h"

#include "nes_gamepad.h"

int main() {
	uint gpState;
	
	// first set the proper clock
	set_sys_clock_khz(141600, true);
	
	// initialize stdio so we can print something via USB
	stdio_init_all();
	
	sleep_ms(2000);
	
	if(gamepadStart(pio0, 6, 8, 7, &gpState) != 0) {
		printf("Oops: failed to initialize gamepad!\n");
		
		while(1) {}
	}
	
	while(1) {
		sleep_ms(200);
		
		// print current state
		if((gpState & 0xff) == 0xff) {
			printf("no buttons pressed\n");
		} else {
			printf("buttons pressed: ");
			
			if(!(gpState & 0x80))
				printf("A ");
			if(!(gpState & 0x40))
				printf("B ");
			if(!(gpState & 0x20))
				printf("select ");
			if(!(gpState & 0x10))
				printf("start ");
			if(!(gpState & 0x8))
				printf("Up ");
			if(!(gpState & 0x4))
				printf("Down ");
			if(!(gpState & 0x2))
				printf("Left ");
			if(!(gpState & 0x1))
				printf("Right ");
			
			printf("\n");
		}
	}
	
	return 0;
}
