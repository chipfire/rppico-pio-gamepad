#include <stdio.h>

#include "hardware/gpio.h"
#include "hardware/pio.h"
#include "hardware/irq.h"

#include "build/nes_gamepad.pio.h"

uint pioProgOffset;
int pioSm;
PIO pioUsed;

uint *gpState;

void gamepadIrqHandler() {
	// the interrupt handler, not much exiting stuff going on here.
	// We just need to clear the interrupt and read the new data from the SM FIFO.
	// clear interrupt first
	pio_interrupt_clear(pioUsed, 0);
	
	// then read current status word from SM FIFO and write it to the configured destination.
	*gpState = pio_sm_get(pioUsed, pioSm);
}

int gamepadStart(PIO pio, uint dataPin, uint clkPin, uint psPin, uint *gpStateVar) {
	gpState = NULL;
	
	if(gpStateVar == NULL)
		return -1;
	
	// try to get a PIO SM, returns -1 if none is available.
	pioSm = pio_claim_unused_sm(pio, false);
	
	// if no SM was available, fail
	if(pioSm < 0)
		return -1;
	
	// check whether the PIO has sufficient program space available. If not, fail.
	if(!pio_can_add_program(pio, &NES_controller_interface_program))
		return -1;

// for debugging	
//	printf("Pins assigned: %u %u %u SM used: %d\n", dataPin, clkPin, psPin, pioSm);
	
	pioUsed = pio;
	gpState = gpStateVar;
	
	pioProgOffset = pio_add_program(pio, &NES_controller_interface_program);
	
	nesControllerProgramInit(pio, (uint) pioSm, pioProgOffset, dataPin, clkPin, psPin);
	
	// set up interrupt handler and source. Making the IRQ configurable would be nice.
	irq_set_exclusive_handler(pio_get_index(pio) ? PIO1_IRQ_0 : PIO0_IRQ_0, gamepadIrqHandler);
	irq_set_enabled(pio_get_index(pio) ? PIO1_IRQ_0 : PIO0_IRQ_0, true);
	irq_set_priority(pio_get_index(pio) ? PIO1_IRQ_0 : PIO0_IRQ_0, PICO_LOWEST_IRQ_PRIORITY);
	pio_set_irq0_source_enabled(pio, pis_interrupt0 + pioSm, true);
	
	// enable the SM, that also starts the program
	pio_sm_set_enabled(pio, pioSm, true);
	
	return 0;
}

// can be used for debugging
uint getPioProgOff() {
	return pioProgOffset;
}

uint getPioSm() {
	return pioSm;
}
