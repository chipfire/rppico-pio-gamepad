#include "hardware/pio.h"

#ifndef NES_GAMEPAD_H__
#define NES_GAMEPAD_H__

int gamepadStart(PIO pio, uint dataPin, uint clkPin, uint psPin, uint *gpStateVar);

uint getPioProgOff();

uint getPioSm();

#endif
